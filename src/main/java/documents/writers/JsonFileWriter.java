package documents.writers;

import documents.exceptions.FileWriterException;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class JsonFileWriter implements IFileWriter {

    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {
        try {
            FileWriter writer = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);

            JSONArray jsonArray = new JSONArray();
            for (Map<String, String> record : data) {
                jsonArray.put(record);
            }
            bufferedWriter.write(jsonArray.toString());
            bufferedWriter.close();
            writer.close();
        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e.getCause());
        }

    }
}
