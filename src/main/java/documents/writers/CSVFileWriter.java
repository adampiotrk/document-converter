package documents.writers;

import documents.exceptions.FileWriterException;

import java.io.*;
import java.util.List;
import java.util.Map;

public class CSVFileWriter implements IFileWriter {

    private static final String CSV_FILE_SEPARATOR = ";";

    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {
        try {
            FileWriter writer = new FileWriter(filePath);
            String rows = prepareRows(data);
            writer.write(rows);
            writer.close();
        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e.getCause());
        }
    }

    private String prepareRows(List<Map<String, String>> data) {
        StringBuilder rows = new StringBuilder();
        Object[] keys = data.get(0).keySet().toArray();
        for (int i = 0; i < keys.length; i++) {
            rows.append(keys[i].toString() + CSV_FILE_SEPARATOR);
        }
        rows.deleteCharAt(rows.length() - 1);
        rows.append('\n');

        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < keys.length; j++) {
                rows.append(data.get(i).get(keys[j]) + CSV_FILE_SEPARATOR);

            }
            rows.deleteCharAt(rows.length() - 1);
            rows.append('\n');
        }
        rows.deleteCharAt(rows.length() - 1);
        return rows.toString();
    }
}
