package documents.readers;

public class FileReaderFactory {
    public IFileReader produce(String filePath) {
         IFileReader result = null;

        if (filePath.endsWith(".csv")) {
            result = new CSVFileReader();
        }
        if (filePath.endsWith(".json")) {
            result = new JsonAlternativeFileReader();
        }

        return result;
    }
}
