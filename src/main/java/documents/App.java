package documents;

import documents.converter.DocumentConverter;
import documents.converter.IDocumentConverter;
import documents.exceptions.FileReaderException;
import documents.exceptions.FileWriterException;

public class App {
    public static void main(String[] args) {
        String inputFilePath = "C:\\Users\\adamk\\IdeaProjects\\documentconverter\\src\\main\\resources\\in.json";
        String outputFilePath = "C:\\Users\\adamk\\IdeaProjects\\documentconverter\\src\\main\\resources\\out.json";

        IDocumentConverter documentConverter = new DocumentConverter();
        try {
            documentConverter.convert(inputFilePath, outputFilePath);
        } catch (FileReaderException e) {
            System.out.println("BŁĄD! WCZYTANIE PLIKU NIE POWIODŁO SIĘ");
        } catch (FileWriterException e) {
            System.out.println("BŁĄD! ZAPIS DO PLIKU NIEUDANY");
        }
        System.out.println(" ");

    }
}
