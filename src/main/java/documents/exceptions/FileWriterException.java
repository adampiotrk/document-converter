package documents.exceptions;

public class FileWriterException extends Exception {
    public FileWriterException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
