package documents.converter;

import documents.exceptions.FileReaderException;
import documents.exceptions.FileWriterException;
import documents.readers.FileReaderFactory;
import documents.readers.IFileReader;
import documents.writers.FileWriterFactory;
import documents.writers.IFileWriter;

import java.util.List;
import java.util.Map;

public class DocumentConverter implements IDocumentConverter {
    @Override
    public void convert(String inputFilePath, String outputFilePath) throws FileReaderException, FileWriterException {
        ///load and read file
        FileReaderFactory fileReaderFactory = new FileReaderFactory();
        IFileReader fileReader = fileReaderFactory.produce(inputFilePath);
        List<Map<String, String>> data = fileReader.read(inputFilePath);

        ///save to file
        FileWriterFactory fileWriterFactory = new FileWriterFactory();
        IFileWriter fileWriter = fileWriterFactory.produce(outputFilePath);
        fileWriter.write(outputFilePath, data);

    }
}
